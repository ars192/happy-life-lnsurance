//
//  MainTabController.swift
//  happy-life-insurance
//
//  Created by Arystan on 4/24/21.
//

import UIKit

protocol MainTabDelegate {
    func updateWorkingStatus(working: Bool)
}


class MainTabController: UITabBarController {

    let actionButton: UIButton = {
            let button = UIButton(type: .system)
            button.tintColor = .white
            button.backgroundColor = .blue
            button.setImage(UIImage(named: "currencyTenge"), for: .normal)
            button.addTarget(self, action: #selector(nextPage), for: .touchUpInside)
            return button
    }()
    
    let dashboard = DashboardController()
    let requests = RequestsController()
    let settings = SettingsController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewControllers()
        configureUI()
    }
    
    func configureUI() {
        view.tintColor = .systemBlue
    }
    
    func configureViewControllers() {
        dashboard.delegate = self
        
        let nav1 = templateNavigationController(image: UIImage(named: "catering"), rootViewController: dashboard)
        nav1.tabBarItem.title = "Dashboard"
        nav1.tabBarItem.badgeValue = ""
        nav1.tabBarItem.badgeColor = .systemRed
        
        
        let nav2 = templateNavigationController(image: UIImage(named: "cart"), rootViewController: requests)
        nav2.tabBarItem.title = "Requests"
        
        let nav3 = templateNavigationController(image: UIImage(named: ""), rootViewController: settings)
        nav3.tabBarItem.title = "Settings"
        
        let controllers = [nav1, nav2, nav3]
        viewControllers = controllers
    }
    
    func templateNavigationController(image: UIImage?, rootViewController: UIViewController) -> UINavigationController {
        let nav = UINavigationController(rootViewController: rootViewController)
        nav.tabBarItem.image = image
        nav.navigationBar.tintColor = .white
        return nav
    }

    @objc
    func nextPage() {
        print("tapped")
    }
}

extension MainTabController: MainTabDelegate {
    func updateWorkingStatus(working: Bool) {
        if working {
            dashboard.tabBarItem.badgeValue = ""
            dashboard.tabBarItem.badgeColor = .systemBlue
        } else {
            dashboard.tabBarItem.badgeValue = ""
            dashboard.tabBarItem.badgeColor = .systemRed
        }
    }
}
