//
//  SettingsController.swift
//  happy-life-insurance
//
//  Created by Arystan on 4/24/21.
//

import UIKit

class SettingsController: UIViewController {

    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    func setupViews() {
        logoutButton.layer.borderWidth = 1
        logoutButton.layer.borderColor = UIColor.systemBlue.cgColor
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        delegate.present(viewController: LoginViewController())
    }
}
