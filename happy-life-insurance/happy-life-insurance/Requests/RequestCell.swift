//
//  RequestCell.swift
//  happy-life-insurance
//
//  Created by Arystan on 4/25/21.
//

import UIKit

class RequestCell: UITableViewCell {

    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
