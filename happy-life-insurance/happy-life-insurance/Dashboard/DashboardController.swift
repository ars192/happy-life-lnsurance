//
//  DashboardController.swift
//  happy-life-insurance
//
//  Created by Arystan on 4/24/21.
//

import UIKit

class DashboardController: UIViewController {
    
    fileprivate var status: Bool = false
    fileprivate var flexStatus: Float = 8
    
    let weeklyHours: Float = 40
    var delegate: MainTabDelegate?
    var data: [Entry] = []
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameSurnameTitle: UILabel!
    @IBOutlet weak var departmentNameLabel: UILabel!
    @IBOutlet weak var workButton: UIButton!
    @IBOutlet weak var flexStatusLabel: UILabel!
    @IBOutlet weak var flexStatusProgressView: UIProgressView!
    @IBOutlet weak var entryTableView: UITableView!
    
    var start: Date?
    var calendarStart: Calendar?
    var end: Date?
    var calendarEnd: Calendar?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    func setupViews() {
        entryTableView.allowsSelection = false
        entryTableView.delegate = self
        entryTableView.dataSource = self
        entryTableView.register(UINib(nibName: "EntryCell", bundle: nil), forCellReuseIdentifier: "EntryCell")
        
        workButton.layer.borderWidth = 1
        workButton.layer.borderColor = UIColor.systemBlue.cgColor
        workButton.layer.cornerRadius = 10
        workButton.layer.masksToBounds = true
        workButton.setTitle("Start", for: .normal)
        workButton.setTitleColor(.systemBlue, for: .normal)
        
        flexStatusLabel.text = "\(flexStatus)/\(weeklyHours)"
        
        print(flexStatus / weeklyHours)
        switch flexStatus / weeklyHours {
        case 0..<0.33:
            flexStatusProgressView.tintColor = .systemRed
        case 0.33..<0.66:
            flexStatusProgressView.tintColor = .systemBlue
        default:
            flexStatusProgressView.tintColor = .systemYellow
        }
        flexStatusProgressView.progress = Float(flexStatus / weeklyHours)
    }
    
    @IBAction func changeWorkingStatusAction(_ sender: UIButton) {
        if status == false {
            status.toggle()
            workButton.layer.borderColor = UIColor.systemRed.cgColor
            workButton.setTitleColor(UIColor.systemRed, for: .normal)
            workButton.setTitle("Stop", for: .normal)
            start = Date()
            calendarStart = Calendar.current
        } else {
            status.toggle()
            workButton.layer.borderColor = UIColor.systemBlue.cgColor
            workButton.setTitleColor(UIColor.systemBlue, for: .normal)
            workButton.setTitle("Start", for: .normal)
            end = Date()
            calendarEnd = Calendar.current
            
            let hourStart = calendarStart!.component(.hour, from: start!)
            let minuteStart = calendarStart!.component(.minute, from: start!)
            
            let hourEnd = calendarEnd!.component(.hour, from: end!)
            let minuteEnd = calendarEnd!.component(.minute, from: end!)
            
            let entry = Entry(id: 0, inTime: "\(hourStart):\(minuteStart)", outTime: "\(hourEnd):\(minuteEnd)", date: end!, description: "MANUAL")
            data.append(entry)
        }
        updateWorkingStatus(working: status)
        entryTableView.reloadData()
    }
    
    func updateWorkingStatus(working: Bool) {
        if working {
            self.navigationController?.tabBarItem.badgeValue = ""
            self.navigationController?.tabBarItem.badgeColor = .systemBlue
        } else {
            self.navigationController?.tabBarItem.badgeValue = ""
            self.navigationController?.tabBarItem.badgeColor = .systemRed
        }
    }
}

extension DashboardController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EntryCell", for: indexPath) as! EntryCell
        let components = data[indexPath.row].date.get(.day, .month, .year)
        cell.dateLabel.text = ("\(components.day!) - \(components.month!) - \(components.year!)")
        cell.descriptionLabel.text = data[indexPath.row].description
        cell.inLabel.text = data[indexPath.row].inTime
        cell.outLabel.text = data[indexPath.row].outTime
        return cell
    }
}
