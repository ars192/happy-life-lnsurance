//
//  Entry.swift
//  happy-life-insurance
//
//  Created by Arystan on 4/25/21.
//

import Foundation

public struct Entry: Decodable {
    let id: Int
    let inTime: String
    let outTime: String
    let date: Date
    let description: String
}
