//
//  Request.swift
//  happy-life-insurance
//
//  Created by Arystan on 4/25/21.
//

import Foundation

public struct Request: Decodable {
    let id: Int
    let description: String
    let date: Date
}
