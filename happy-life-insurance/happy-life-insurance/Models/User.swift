//
//  User.swift
//  happy-life-insurance
//
//  Created by Arystan on 4/25/21.
//

import Foundation

public struct User: Decodable {
    let id: Int
    let name: String
    let surname: String
    let email: String
    let password: String
}
