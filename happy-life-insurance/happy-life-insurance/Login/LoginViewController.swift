//
//  LoginViewController.swift
//  happy-life-insurance
//
//  Created by Arystan on 4/22/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func didTouchLoginButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Error", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))

//        let date = Date().dayNumberOfWeek()
//        if date! / 1 == 1 || date! / 7 == 1 {
//            alert.message = "System not working on holidays"
//            self.present(alert, animated: true)
//            return
//        }
        
        if emailField.text == "test" && passwordField.text == "123" {
            guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            delegate.present(viewController: MainTabController())
        } else {
            alert.message = "incorrect email or password"
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func didTouchForgotButton(_ sender: UIButton) {
        print("proceed to forgot password view")
    }
    
}
